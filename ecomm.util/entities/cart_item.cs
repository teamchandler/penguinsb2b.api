﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class cart_item
    {
        public string cart_item_id { get; set; }
        //public string _id { get; set; }
        public string id { get; set; }
        public string sku { get; set; }
        public string brand { get; set; }
        public int quantity { get; set; }
        public double mrp { get; set; }
        public double final_offer { get; set; }
        public double sub_total { get; set; }
        public string name { get; set; }
        public imgurl image { get; set; }
        public double discount { get; set; }
        public bool express { get; set; }
        public List<size> sizes { get; set; }
        public string selected_size { get; set; }

        public string item_status { get; set; }
        public int delivered_qty { get; set; }
        public int shipping_qty { get; set; }

    }

    
    public class cart_items
    {
        public parent_cart_items parent_prod_detail { get; set; }
        public List<child_cart_items> child_prod_detail { get; set; }
        public int order_quantity { get; set; }
        public double order_amount { get; set; }
       

    }
    
  
     public class Cat_Id
    {
        public string cat_id{get; set;}
                        
    }

     public class product_details
     {
         public string id { get; set; }
         public string size { get; set; }
         public string sku { get; set; }
         public string ord_qty { get; set; }
     }
}
