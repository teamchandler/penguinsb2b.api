﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class registration
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string office_no { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string password { get; set; }
        public string new_password { get; set; }
        public int status { get; set; }
        public string location { get; set; }
        public string user_status { get; set; }
        public string registration_date { get; set; }
        public string token_id { get; set; }
        public string message { get; set; }
        public string total_point { get; set; }
        public string company { get; set; }
        public decimal points { get; set; }
        public decimal max_point_range { get; set; }
        public decimal min_point_range { get; set; }


        public long Total_Page { get; set; }
        public long Total_Records { get; set; }

        /* 0 : Applied for Registration
         * 1 : Pending For Activation [Email Sent]
         * 2 : Activated
         * 3 : Suspended/Terminated
         * 4 : Inactive
         */
    }



    public class registration_active
    {
        public string email_id { get; set; }
        public DateTime activationdate { get; set; }
        public string guid { get; set; }

    }

    public class user_login
    {
        public string email_id { get; set; }
        public string password { get; set; }
        public DateTime logindate { get; set; }
        public string company { get; set; }
    }

    public class registration_history
    {
        public string email_id { get; set; }
        public DateTime actiondate { get; set; }
        public int status { get; set; }
        /* 
         * 1 : Pending For Activation [Email Sent]
         * 2 : Activated
         * 3 : Suspended/Terminated
         * 4 : Inactive
         */
    }

    public class user_security
    {
        public long Id { get; set; }
        public string user_name { get; set; }
        public long company_id { get; set; }
        public string password { get; set; }
        public string user_type { get; set; }
    }

    public class user_rigths
    {
        public Int64 user_right_id { get; set; }
        public string user_right_desc { get; set; }
        public long USER_ID { get; set; }
    }

    public class user_assign_rigths
    {
        public List<long> RIGHTS_IDS { get; set; }
        public long USER_ID { get; set; }
        public string created_by { get; set; }
    }

    public class user_credential
    {
        public List<String> emailids { get; set; }
    }

    public class expo_cust_grid
    {
        public List<cust_grid_data> GridData { get; set; }

    }
    public class cust_grid_data
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email_id { get; set; }
        public string user_status { get; set; }
        public string password { get; set; }
        public string location { get; set; }
    }

    public class migrate_store
    {
        public string store { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string emailid { get; set; }
        public string pointsloaded { get; set; }
        public string pointsredeemed { get; set; }
        public string shippingaddress { get; set; }
    }



    public class sales_target
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string office_no { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        public string location { get; set; }
        public string user_status { get; set; }
        public string registration_date { get; set; }
        public string token_id { get; set; }
        public string message { get; set; }
        public string total_point { get; set; }
        public string company { get; set; }
        public decimal max_point_range { get; set; }
        public decimal min_point_range { get; set; }

        public string code { get; set; }

        public string Mother_Maiden_Name { get; set; }
        public string First_School { get; set; }
        public string Native_Place { get; set; }
        public string Retailer { get; set; }
        public string Date_Of_Birth { get; set; }
        public string Target { get; set; }
        public string Achievement { get; set; }
        public string Points { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }



    }

    public class sales_target_monthly_lava
    {

        public string user_id { get; set; }
        public string sales_cycle_id { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Sku { get; set; }
        public string TargetValue { get; set; }
        public string AchieveValue { get; set; }
        public string TargetVol { get; set; }
        public string AchieveVol { get; set; }
        public string Points { get; set; }
        public string source { get; set; }
        public string create_ts { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }


    }

    public class Lava_Data
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string DateOfJoiningLAVA { get; set; }
        public string Qualifications { get; set; }
        public string DSEContactNo { get; set; }
        public string DSEEMailId { get; set; }
        public string DSEPostalAddress { get; set; }
        public string DecValueAchivement { get; set; }
        public string BranchManager { get; set; }
        public string BranchName { get; set; }
        public string Status { get; set; }
        public string DSEPoints { get; set; }
        public string CompanyId { get; set; }

        public string SelesCycleId { get; set; }


    }

    public class Xolo_Data
    {

        public string Emp_Id { get; set; }
        public string BU { get; set; }
        public string Branch { get; set; }
        public string Outlet_Code { get; set; }
        public string Outlet_Name { get; set; }
        public string FOS_DSE_CODE { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string FOS_DSE_Mail_Id { get; set; }
        public string DSE_Contact_Number { get; set; }
        public string TM_Name { get; set; }
        public string TM_Code { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string DOB { get; set; }
        public string DOJ { get; set; }
        public string Qualifiations { get; set; }
        public string DecValueAchivement { get; set; }
        public string Status { get; set; }
        public string DSEPoints { get; set; }
        public string CompanyId { get; set; }
        public string SelesCycleId { get; set; }

    }

    public class Puma_data
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string office_no { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        public string location { get; set; }
        public string user_status { get; set; }
        public string registration_date { get; set; }
        public string token_id { get; set; }
        public string message { get; set; }
        public string total_point { get; set; }
        public string company { get; set; }
        public string CompanyId { get; set; }

        public string email_address { get; set; }
        public string company_name { get; set; }
        public string designation { get; set; }
        public string partner_id { get; set; }
        public string zone { get; set; }
        public string district { get; set; }
        public string outlet_name { get; set; }
        public string outlet_phone { get; set; }
    }

    public class store_message
    {
        public long Msg_id { get; set; }
        public string Store_id { get; set; }
        public string Msg_content { get; set; }
        public string Msg_type { get; set; }
        public string Subject { get; set; }

    }


    public class warehouse
    {

        public string warehouse_name { get; set; }
        public string owner_name { get; set; }
        public string owner_email { get; set; }
        public string warehouse_address { get; set; }
        public DateTime created_ts { get; set; }

    }


    public class product_enquire : enquire
    {
        public long id { get; set; }
        public string quantity { get; set; }
        public long total_enq_qty { get; set;}
        public string logo { get; set; }
        public string name { get; set; }
        public string mobile_no { get; set; }
        public string email_id { get; set; }
        public string city { get; set; }
        public string company_name { get; set; }
        public string website { get; set; }
        public string enquire_from { get; set; }
        public string prod_id { get; set; }
        public string prod_name { get; set; }
        public string size_quantity { get; set; }
        public string created_by { get; set; }
        public string status { get; set; }
        public string store { get; set; }
        public string comments { get; set; }
        public long total_enq_qty_wise_amount { get; set; }
        public List<product_enquire_with_product_details> pe { get; set; }
    
    }

    public class product_enquire_with_product_details : product_enquire
    {
        public string name { get; set; }
        public string prod_id { get; set; }
        public string size { get; set; }
        public string sku { get; set; }
        public string ord_qty { get; set; }
    }


    public class enquire
    {
        public long id { get; set; }
        public string contact_as { get; set; }
        public string mobile_no { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string operating_city { get; set; }
        public string company_name { get; set; }
        public string website { get; set; }
        public string message { get; set; }
        public DateTime create_ts { get; set; }
        public string enquire_from { get; set; }
        public string general_information { get; set; }
        public string product_code { get; set; }
        public string additional_remarks { get; set; }
        public string created_by { get; set; }
        public string comment { get; set; }
        public string status { get; set; }

    }

    public class gen_enq_upd
    {
        public List<int> enqids { get; set; }
        public int status { get; set; }
    }
	


}
