﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
   public class stock_list
    {
        public string warehouse { get; set; }
        public int stock { get; set; }
        public int blocked_stock { get; set; }
    }
}
