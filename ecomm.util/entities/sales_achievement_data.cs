﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class sales_achievement_data
    {
        public string cycle_name { get; set; }
        public int sales_cycle_id { get; set; }
        public string sku { get; set; }
        public int target_val { get; set; }
        public int achieve_val { get; set; }
        public int target_vol { get; set; }
        public int achieve_vol { get; set; }
        public int point { get; set; }

    }
}
