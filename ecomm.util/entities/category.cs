﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class category:cate
    {
        public List<imgurl> image_urls { get; set; }
        public List<url> video_urls { get; set; }
        public List<adurl> ad_urls { get; set; }
        public string number_items { get; set; }
    }
    public class cat_new
    {

        public string brand { get; set; }
        public List<cat_brand> brand_cat_associate { get; set; }
      
    
    }

    public class cat_brand
    {
       public string brand { get; set; }
       public string catlevel_1 { get; set; }
       public string catlevel_2 { get; set; }
       public string catlevel_3 { get; set; }
       public string catlevel3_name { get; set; }
       public string catlevel1_name { get; set; }
       public string catlevel2_name { get; set; }
    
    
    }

    public class cat_ids
    {
        public string catlevel_1 { get; set; }
        public string catlevel_2 { get; set; }
    
    }


}
