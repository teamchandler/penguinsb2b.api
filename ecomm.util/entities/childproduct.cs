﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class childproduct
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string id { get; set; }
        public string parent_prod { get; set; }
        public string size { get; set; }
        public string sku { get; set; }
        public List<bulk_stock> stock { get; set; }
        public string Express { get; set; }
    }
}
