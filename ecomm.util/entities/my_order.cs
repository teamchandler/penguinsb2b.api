﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
   public class my_order
    {
       public int order_id { get; set; }
       public string billing_name { get; set; }
       public string sales_person_name { get; set; }
       public double total_amount { get; set; }
       public string payment_info { get; set; }

    }
}
