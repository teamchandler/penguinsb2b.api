﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Newtonsoft.Json;

namespace ecomm.util.entities
{
    public class menu_item
    {
        public string name { get; set; }
        public string link { get; set; }
        public int level { get; set; }
        public string ParentId { get; set; }
        

    }
}
