﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
using Newtonsoft.Json;
// to be removed
using MongoDB.Bson;
using MongoDB.Driver;
using System.Data;


namespace ecomm.model.repository
{
    public class store_repository
    {

        #region get methods


        public discount_coupon validate_discount_code(string discount_code, string store, string user)
        {

            DataAccess da = new DataAccess();

            discount_coupon coupon = new discount_coupon();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_discount_coupon"
                                , da.Parameter("_discount_code", discount_code)
                                , da.Parameter("_store", store)
                                , da.Parameter("_user_id", user)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    coupon.discount_code = discount_code;
                    coupon.rule = convertToString(dtResult.Rows[0]["rule"].ToString());
                    coupon.content_url = convertToString(dtResult.Rows[0]["content_url"].ToString());
                }

                return coupon;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private MongoCursor get_company_cat_exclusion(string company)
        {
            dal.DataAccess dal = new DataAccess();
            //string q = "{'ParentId': { $ne:'0'}}";
            string q = "{'company':'" + company + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "cat", "brand" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("company_exclusion", queryDoc, include_fields, exclude_fields);
            return cursor;
        }

        public List<menu> get_menu(string company)
        {
            // get company exclusion
            string excl_cat = "";
            string excl_brand = "";
            if (company != "")
            {
                MongoCursor cursor_exclusion = get_company_cat_exclusion(company);
                if (cursor_exclusion.Count() > 0)
                {
                    List<string> exclude_cat = new List<string>();
                    excl_cat = "[";
                    foreach (var c in cursor_exclusion)
                    {
                        for (int i = 0; i < c.ToBsonDocument()["cat"].AsBsonArray.Count; i++)
                        {
                            excl_cat = excl_cat + "'" + c.ToBsonDocument()["cat"].AsBsonArray[i].ToString() + "',";
                        }
                        excl_cat = excl_cat + "]";
                    }
                    List<string> exclude_brand = new List<string>();
                    excl_brand = "[";
                    foreach (var c in cursor_exclusion)
                    {
                        if (check_field(c.ToBsonDocument(), "brand"))
                        {
                            for (int i = 0; i < c.ToBsonDocument()["brand"].AsBsonArray.Count; i++)
                            {
                                excl_brand = excl_brand + "'" + c.ToBsonDocument()["brand"].AsBsonArray[i].ToString() + "',";
                            }
                        }
                        excl_brand = excl_brand + "]";
                    }

                }
            }
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //string q = "{'ParentId': { $ne:'0'}}";
            string q = "";
            if (excl_cat != "")
            {
                q = "{'active': 1, 'id' : {$nin :" + excl_cat + "}}}";
            }
            else
            {
                q = "{'active': 1}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "Name", "id", "ParentId" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
            var menu = new List<menu>();
            List<menu_item> menu_items = new List<menu_item>();
            foreach (var c in cursor)
            {
                menu_item m = new menu_item();
                m = JsonConvert.DeserializeObject<menu_item>(c.ToBsonDocument().ToString());
                m.link = c.ToBsonDocument()["id"].ToString();
                m.ParentId = c.ToBsonDocument()["ParentId"].ToString();
                var tmp = c.ToBsonDocument()["id"].ToString().Split('.');
                if (tmp.Length == 3)
                {
                    if (tmp[2] != "0")
                    {
                        m.level = 3;
                    }
                    else
                    {
                        m.level = 2;
                    }
                }
                else
                {
                    m.level = 1;
                }
                menu_items.Add(m);
            }

            foreach (menu_item m in menu_items)
            {
                switch (m.level)
                {
                    case 1:
                        menu menu_head = new menu();
                        menu_head.name = m.name.ToLower();
                        menu_head.id = m.link;
                        List<menu_item> menu_l2 = new List<menu_item>();
                        menu_l2 = menu_items.FindAll(delegate(menu_item m_in) { return m_in.ParentId.ToString() == m.link.ToString(); });
                        menu_l2.Remove(m);
                        List<menu_item> menu_l3 = new List<menu_item>();
                        foreach (menu_item m2 in menu_l2)
                        {
                            var menu_l3_tmp = (List<menu_item>)menu_items.FindAll(delegate(menu_item m_in) { return m_in.ParentId.ToString() == m2.link.ToString(); });
                            menu_l3.AddRange(menu_l3_tmp);
                        }

                        //menu_l3.Remove(m);
                        List<menu_item> sub_menu_items = new List<menu_item>();
                        menu_l2.AddRange(menu_l3);
                        menu_head.menu_items = menu_l2;
                        menu.Add(menu_head);
                        break;
                }

            }
            //return mongo_query_singleton (q, "cart", include_fields, exclude_fields);
            return menu;
        }


        public List<menu> get_advance_menu(string company)
        {


            // get company exclusion
            string excl_cat = "";
            string excl_brand = "";

            try
            {

                if (company != "")
                {
                    MongoCursor cursor_exclusion = get_company_cat_exclusion(company);
                    if (cursor_exclusion.Count() > 0)
                    {
                        List<string> exclude_cat = new List<string>();
                        excl_cat = "[";
                        foreach (var c in cursor_exclusion)
                        {
                            for (int i = 0; i < c.ToBsonDocument()["cat"].AsBsonArray.Count; i++)
                            {
                                excl_cat = excl_cat + "'" + c.ToBsonDocument()["cat"].AsBsonArray[i].ToString() + "',";
                            }
                            excl_cat = excl_cat + "]";
                        }
                        List<string> exclude_brand = new List<string>();
                        excl_brand = "[";
                        foreach (var c in cursor_exclusion)
                        {
                            if (check_field(c.ToBsonDocument(), "brand"))
                            {
                                for (int i = 0; i < c.ToBsonDocument()["brand"].AsBsonArray.Count; i++)
                                {
                                    excl_brand = excl_brand + "'" + c.ToBsonDocument()["brand"].AsBsonArray[i].ToString() + "',";
                                }
                            }
                            excl_brand = excl_brand + "]";
                        }

                    }
                }
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                //string q = "{'ParentId': { $ne:'0'}}";
                string q = "";
                if (excl_cat != "")
                {
                    q = "{'active': 1, 'id' : {$nin :" + excl_cat + "}}}";
                }
                else
                {
                    q = "{'active': 1}";
                }
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "Name", "id", "ParentId" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
                var menu = new List<menu>();
                List<menu_item> menu_items = new List<menu_item>();
                foreach (var c in cursor)
                {
                    menu_item m = new menu_item();
                    m = JsonConvert.DeserializeObject<menu_item>(c.ToBsonDocument().ToString());
                    m.link = c.ToBsonDocument()["id"].ToString();
                    m.ParentId = c.ToBsonDocument()["ParentId"].ToString();
                    var tmp = c.ToBsonDocument()["id"].ToString().Split('.');
                    if (tmp.Length == 3)
                    {
                        if (tmp[2] != "0")
                        {
                            m.level = 3;
                        }
                        else
                        {
                            m.level = 2;
                        }
                    }
                    else
                    {
                        m.level = 1;
                    }
                    menu_items.Add(m);
                }




                foreach (menu_item m in menu_items)
                {
                    switch (m.level)
                    {
                        case 1:
                            menu menu_head = new menu();
                            menu_head.name = m.name.ToLower();
                            menu_head.id = m.link;
                            List<menu_item> menu_l2 = new List<menu_item>();
                            menu_l2 = menu_items.FindAll(delegate(menu_item m_in) { return m_in.ParentId.ToString() == m.link.ToString(); });
                            menu_l2.Remove(m);
                            List<menu_item> menu_l3 = new List<menu_item>();
                            foreach (menu_item m2 in menu_l2)
                            {
                                var menu_l3_tmp = (List<menu_item>)menu_items.FindAll(delegate(menu_item m_in) { return m_in.ParentId.ToString() == m2.link.ToString(); });
                                menu_l3.AddRange(menu_l3_tmp);
                            }

                            //menu_l3.Remove(m);
                            List<menu_item> sub_menu_items = new List<menu_item>();
                            menu_l2.AddRange(menu_l3);
                            menu_head.menu_items = menu_l2;
                            menu.Add(menu_head);
                            break;
                    }

                }

                string readytoship_qry = "{'active' : 1, 'parent_prod' : '0', 'Ready_to_ship' : '1' }";

                int DateRange = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["DateRange"].ToString());
                var EndDate = "'" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                var StartDate = "'" + System.DateTime.UtcNow.AddDays(-DateRange).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                string new_qry = q = "{'active' : 1, 'parent_prod' : '0', 'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")}}";

                string sort_by_price_qry = "{'active' : 1, 'parent_prod' : '0'}";

                List<menu_item> ol3rdevelreadymenu = get_advance_level3_menu(menu, readytoship_qry, "ready to ship");
                List<menu_item> ol3rdlevelnewmenu = get_advance_level3_menu(menu, new_qry, "new");
                List<menu_item> ol3rdlevelnewsortbyprice = get_advance_level3_menu(menu, sort_by_price_qry, "sort by price");

                if (menu.Count() > 0)
                {

                    if (ol3rdevelreadymenu.Count() > 0)
                    {
                        for (int i = 0; i < menu.Count(); i++)
                        {
                            if (menu[i].name == "ready to ship")
                            {
                                menu[i].menu_items.AddRange(ol3rdevelreadymenu);
                            }
                        }
                    }
                    if (ol3rdlevelnewmenu.Count() > 0)
                    {
                        for (int i = 0; i < menu.Count(); i++)
                        {
                            if (menu[i].name == "new")
                            {
                                menu[i].menu_items.AddRange(ol3rdlevelnewmenu);
                            }
                        }
                    }
                    if (ol3rdlevelnewsortbyprice.Count() > 0)
                    {
                        for (int i = 0; i < menu.Count(); i++)
                        {
                            if (menu[i].name == "sort by price")
                            {
                                menu[i].menu_items.AddRange(ol3rdlevelnewsortbyprice);
                            }
                        }
                    }
                }







                return menu;
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        public List<menu_item> get_advance_level3_menu(List<menu> olm, string q, string special_menu_name)
        {

            List<menu_item> lmenu = new List<menu_item>();

            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();

            try
            {

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);

                string[] include_fields = get_prod_short_include_fields();
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);

                double min_quantity = 1001;

                List<prod_cat> oppc = new List<prod_cat>();

                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod = JsonConvert.DeserializeObject<product>(c.ToBsonDocument().ToString());
                    if (prod.price.Count() > 0 && prod.parent_cat_id != null && prod.parent_cat_id != "")
                    {
                        List<price> prod_price = new List<price>();
                        prod_price = prod.price.FindAll(delegate(price m_in) { return m_in.min_qty == min_quantity; });
                        if (prod_price.Count() > 0)
                        {
                            if (prod_price[0].final_offer != 0)
                            {
                                get_prod_wise_3_level(prod_price[0].final_offer, ref oppc, prod, olm);
                            }
                        }

                    }
                }

                if (oppc.Count() > 0)
                {
                    var distict_category = oppc.Select(x => x.category).Distinct().ToList();
                    var distict_level1_cat = oppc.Select(x => x.level1_cat_id).Distinct().ToList();

                    List<prod_cat> occ = new List<prod_cat>();

                    if (distict_category.Count() > 0)
                    {
                        for (int l = 0; l < distict_category.Count(); l++)
                        {
                            List<prod_cat> opcl = new List<prod_cat>();
                            opcl = oppc.FindAll(delegate(prod_cat m_in) { return (m_in.category == distict_category[l].ToString()); });

                            for (int m = 0; m < distict_level1_cat.Count(); m++)
                            {
                                List<prod_cat> olpc = new List<prod_cat>();
                                olpc = opcl.FindAll(delegate(prod_cat op) { return (op.level1_cat_id == distict_level1_cat[m].ToString()); });
                                if (olpc.Count() > 0)
                                {
                                    occ.Add(olpc[0]);
                                }
                            }

                        }
                    }


                    List<menu> special_menus = new List<menu>();
                    special_menus = olm.FindAll(delegate(menu menu_in) { return (convertToString(menu_in.name) == convertToString(special_menu_name)); });


                    if (occ.Count() > 0 && special_menus.Count() > 0)
                    {
                        for (int n = 0; n < occ.Count(); n++)
                        {
                            menu_item om = new menu_item();
                            om.level = 3;
                            om.link = occ[n].level1_cat_id;
                            List<menu> omenuname = new List<menu>();
                            omenuname = olm.FindAll(delegate(menu menu_in) { return (convertToString(menu_in.id) == convertToString(occ[n].level1_cat_id)); });
                            om.name = omenuname[0].name.ToUpper();
                            List<menu_item> omenuid = new List<menu_item>();
                            omenuid = special_menus[0].menu_items.FindAll(delegate(menu_item menu_item_in) { return (convertToString(menu_item_in.name) == convertToString(occ[n].category)); });
                            om.ParentId = omenuid[0].link;
                            lmenu.Add(om);
                        }
                    }

                }

                return lmenu;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<prod_cat> get_prod_wise_3_level(double prod_final_price, ref List<prod_cat> oppc, product prod, List<menu> olm)
        {
            try
            {
                var tmp = prod.parent_cat_id.ToString().Split('.');
                string level2_cat_id = "";
                string level1_cat_id = "";
                if (tmp.Length == 3)
                {
                    level2_cat_id = tmp[0].ToString().Trim() + "." + tmp[1].ToString().Trim() + "." + "0";
                    level1_cat_id = tmp[0].ToString().Trim() + "." + "0";
                    //level1_cat_id = get_category_details(level2_cat_id).ParentId;
                }

                prod_cat op = new prod_cat();
                if (prod_final_price >= 1 && prod_final_price <= 19)
                {
                    //op.price = prod_final_price;
                    op.category = "1-19";
                    //op.parent_cat_id = prod.parent_cat_id;
                }
                else if (prod_final_price >= 20 && prod_final_price <= 99)
                {
                    //op.price = prod_final_price;
                    op.category = "20-99";
                    //op.parent_cat_id = prod.parent_cat_id;
                }
                else if (prod_final_price >= 100 && prod_final_price <= 499)
                {
                    //op.price = prod_final_price;
                    op.category = "100-499";
                    //op.parent_cat_id = prod.parent_cat_id;
                }
                else if (prod_final_price >= 500 && prod_final_price <= 999)
                {
                    //op.price = prod_final_price;
                    op.category = "500-999";
                    //op.parent_cat_id = prod.parent_cat_id;
                }
                else if (prod_final_price >= 1000 && prod_final_price <= 2999)
                {
                    //op.price = prod_final_price;
                    op.category = "1000-2999";
                    //op.parent_cat_id = prod.parent_cat_id;
                }
                else if (prod_final_price >= 3000 && prod_final_price <= 9999)
                {
                    //op.price = prod_final_price;
                    op.category = "3000-9999";
                    //op.parent_cat_id = prod.parent_cat_id;
                }
                else if (prod_final_price >= 10000)
                {
                    //op.price = prod_final_price;
                    op.category = ">10000";
                    //op.parent_cat_id = prod.parent_cat_id;
                }

                //op.level2_cat_id = level2_cat_id;
                op.level1_cat_id = level1_cat_id;
                if (op.category != "" && op.category != null)
                {
                    oppc.Add(op);
                }

                return oppc;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public category get_category_details(string cat_id)
        {

            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = "";
                q = "{'active': 1, 'id' : '" + cat_id + "'}";

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "Name", "id", "ParentId" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
                category cat = new category();
                foreach (var c in cursor)
                {
                    cat = JsonConvert.DeserializeObject<category>(c.ToBsonDocument().ToString());
                    break;
                }

                return cat;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int get_menu_expiry_status(string last_download_date)
        {
            int expired = 1;
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                //string q = "{'ParentId': { $ne:'0'}}";
                string q = "";
                string[] include_fields = new string[] { "expired" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("expiry", include_fields, exclude_fields);

                foreach (var c in cursor)
                {
                    string last_menu_update_date = c.ToBsonDocument()["expired"].ToString();
                    DateTime dt_last_download_date = Convert.ToDateTime(last_download_date);
                    DateTime dt_last_menu_update_date = Convert.ToDateTime(last_menu_update_date);
                    if ((dt_last_download_date - dt_last_menu_update_date).TotalDays > 0)
                    {
                        expired = 0;
                    }
                    else
                    {
                        expired = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return expired;
        }
        public List<menu_item> get_level1_menu()
        {
            List<menu_item> menu_items = new List<menu_item>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                //string q = "{'ParentId': { $ne:'0'}}";
                string q = "{'active': 1, 'ParentId':'0'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "Name", "id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
                var menu = new List<menu>();

                foreach (var c in cursor)
                {
                    menu_item m = new menu_item();
                    m = JsonConvert.DeserializeObject<menu_item>(c.ToBsonDocument().ToString());
                    m.link = c.ToBsonDocument()["id"].ToString();
                    m.ParentId = "0";
                    if (m.name != "Airtel Special" && m.name != "Exide Specials")
                    {
                        menu_items.Add(m);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return menu_items;
        }


        public string[] get_cat_list()
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            MongoCursor cursor = dal.execute_mongo_db("category");
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToJson());
            }
            return list.ToArray();
        }


        public List<cat> get_cat_list_short()
        {
            var catagories = new List<cat>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("cat");


                foreach (var c in cursor)
                {
                    cat cat = new cat();
                    cat.Id = c.ToBsonDocument()["_id"].ToString();
                    cat.cat_name = c.ToBsonDocument()["cat_name"].ToString();
                    cat.parent_cat_id = c.ToBsonDocument()["parent_cat_id"].ToString();
                    catagories.Add(cat);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return catagories;
        }
        public List<catagory> get_active_cat_list()
        {
            var catagories = new List<catagory>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{ active : '1'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("cat", queryDoc);


                foreach (var c in cursor)
                {
                    catagory cat = new catagory();
                    cat.Id = c.ToBsonDocument()["_id"].ToString();
                    cat.active = c.ToBsonDocument()["active"].ToString();
                    cat.cat_name = c.ToBsonDocument()["cat_name"].ToString();
                    cat.description = c.ToBsonDocument()["description"].ToString();
                    cat.parent_cat_id = c.ToBsonDocument()["parent_cat_id"].ToString();
                    cat.image_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["image_urls"].ToString());
                    cat.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    cat.ad_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["ad_urls"].ToString());
                    catagories.Add(cat);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return catagories;
        }
        public List<cat> get_active_cat_list_short()
        {
            // evoke mongodb connection method in dal
            var catagories = new List<cat>();

            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{ active : '1'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("cat", queryDoc);


                foreach (var c in cursor)
                {
                    cat cat = new cat();
                    cat.Id = c.ToBsonDocument()["_id"].ToString();
                    cat.cat_name = c.ToBsonDocument()["cat_name"].ToString();
                    cat.parent_cat_id = c.ToBsonDocument()["parent_cat_id"].ToString();
                    catagories.Add(cat);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return catagories;
        }
        public string[] get_cat_details(string cat_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{ active : 1, id:'" + cat_id + "'}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "Name", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();
        }
        public string[] get_cat_list_filters(string cat_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{ active : 1, id:'" + cat_id + "'}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "Name", "filters" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();
        }
        public List<filter> get_newarrivals_items_filters()
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                //string q = "{ 'active' : 1, 'price.list':{$gt:" + premium_threshhold + "}}";

                int DateRange = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["DateRange"].ToString());

                var EndDate = "'" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                var StartDate = "'" + System.DateTime.UtcNow.AddDays(-DateRange).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                //string q = "{ $and: [{'active' : 1},{'create_date':{$lte:" + EndDate + "}},{'create_date':{$gte:" + StartDate + "}}]}";
                string q = "{'active' : 1, 'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")}}";

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }


        }

        public List<filter> get_premium_items_filters(int premium_threshhold)
        {

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'price.list':{$gt:" + premium_threshhold + "}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters", "Name" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    //filter cat_filter = new filter();

                    //List<string> cat_val_list = new List<string>();;
                    foreach (var c in cursor)
                    {
                        // build cat list
                        //cat_filter.values.Add(c.ToBsonDocument()["Name"].ToString());
                        //cat_val_list.Add(c.ToBsonDocument()["Name"].ToString());
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    //cat_filter = new filter() { name = "cat", values = cat_val_list };
                    //list.Add(cat_filter);
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }


        }
        public List<filter> get_special_item_filters_parent(string cat_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'cat_id.cat_id':{$gt:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }
        public List<filter> get_special_item_filters_specific(string cat_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'cat_id.cat_id':'" + cat_id + "'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }
        public string[] get_prod_list_by_cat_short(string cat_id, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            List<String> list = new List<string>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "stock", "have_child", "shipping_days" };
                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);


                foreach (var c in cursor)
                {

                    list.Add(c.ToBsonDocument().ToString());
                    //foreach(var d in c.ToBsonDocument()["cat_id"].AsBsonArray[0].ToBsonDocument()["id"].ToString()
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");

            }
            return list.ToArray();
        }

        public string[] get_prod_list_by_cat_short(string cat_id)
        {
            List<String> list = new List<string>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q;
                //if (cat_id.Last() == '0')
                if (cat_id.EndsWith(".0"))
                {
                    string tmp = cat_id.Substring(0, cat_id.Length - 2);
                    //q = "{'active':1, 'cat_id.cat_id':/^" + tmp + "./}}";
                    q = "{'active':1,'cat_id.cat_id':/^" + tmp + "./}}";
                }
                else
                {
                    //q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                    q = "{'active':1, cat_id:{cat_id:'" + cat_id + "'}}";
                }
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = get_cat_short_include_fields();
                // new string[] {    "id", "brand", "price", 
                //"cat_id", "sku", "point", 
                //"Name", "description", 
                //"image_urls", "feature", 
                //"have_child", "Express" };

                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);


                foreach (var c in cursor)
                {

                    list.Add(c.ToBsonDocument().ToString());
                    //foreach(var d in c.ToBsonDocument()["cat_id"].AsBsonArray[0].ToBsonDocument()["id"].ToString()
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");

            }
            return list.ToArray();

        }
        public string[] get_prod_list_by_cat_short(string cat_id, int min, int max)
        {
            List<String> list = new List<string>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q;
                //if (cat_id.Last() == '0')
                if (min > max) // wrong case
                {
                    // reset to default and ignore
                    min = 0;
                    max = 0;
                }
                if ((min > 0) || (max > 0))
                {

                    if (cat_id.EndsWith(".0"))
                    {
                        string tmp = cat_id.Substring(0, cat_id.Length - 2);
                        //q = "{'active':1, 'cat_id.cat_id':/^" + tmp + "./}}";
                        q = "{'active':1,'cat_id.cat_id':/^" + tmp + "./ , 'price.list':{ $lte: " + max + ", $gte: " + min + "  }}";
                    }
                    else
                    {
                        //q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                        q = "{'active':1, cat_id:{cat_id:'" + cat_id + "'}, 'price.list':{ $lte: " + max + ", $gte: " + min + " }}";
                    }
                }
                else
                {
                    if (cat_id.EndsWith(".0"))
                    {
                        //string tmp = cat_id.Substring(0, cat_id.Length - 2);
                        ////q = "{'active':1, 'cat_id.cat_id':/^" + tmp + "./}}";
                        //q = "{'active':1,'cat_id.cat_id':/^" + tmp + "./}";

                        List<String> level3_cat_list = new List<String>();
                        level3_cat_list.Add(convertToString(cat_id));

                        string q1 = "{'active': 1,'ParentId': '" + cat_id + "'}";
                        MongoCursor curs = get_category_data(q1);
                        
                        foreach (var c in curs)
                        {
                            category cat = new category();
                            cat = JsonConvert.DeserializeObject<category>(c.ToBsonDocument().ToString());
                            level3_cat_list.Add(convertToString(cat.id));
                        }

                        string q2 = "{'active': 1, 'cat_id.cat_id':{$in:[";
                        q = generate_query(q2, level3_cat_list);
                    }
                    else
                    {
                        //q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                        q = "{'active':1, cat_id:{cat_id:'" + cat_id + "'}}";
                    }
                }

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = get_cat_short_include_fields();

                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);


                foreach (var c in cursor)
                {
                    if (c.ToBsonDocument()["have_child"] > 0)
                    {
                        c.ToBsonDocument()["stock"] = -9999;// hardcoded -negatve value to show products with children. system is prone to a situation of we get to -9999 stock - very unlikely
                    }
                    else
                    {
                        c.ToBsonDocument()["stock"] = c.ToBsonDocument()["stock"].ToInt32();
                    }
                    list.Add(c.ToBsonDocument().ToString());
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list.ToArray();

        }


        public string[] get_prod_list_by_special_cat_short(string menu_type, string price_range, string cat_id, int min, int max)
        {
            List<String> list = new List<string>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                List<String> level2_cat_list = new List<String>();
                List<String> level3_cat_list = new List<String>();

                int min_price = 0;
                int max_price = 0;

                int cat_id_length = 0;
                string[] cat_id_arr;
                cat_id_arr = cat_id.Split('.');
                cat_id_length = cat_id_arr.Length;
                if (cat_id_length == 2)
                {

                    string q1 = "{'active': 1,'ParentId': '" + cat_id + "'}";
                    MongoCursor curs = get_category_data(q1);

                    foreach (var c in curs)
                    {
                        category cat = new category();
                        cat = JsonConvert.DeserializeObject<category>(c.ToBsonDocument().ToString());
                        level2_cat_list.Add(convertToString(cat.id));
                    }

                    string q2 = "{'active': 1, 'ParentId':{$in:[";

                    string q3 = generate_query(q2, level2_cat_list);

                    MongoCursor cur = get_category_data(q3);

                    foreach (var c in cur)
                    {
                        category cat = new category();
                        cat = JsonConvert.DeserializeObject<category>(c.ToBsonDocument().ToString());
                        level3_cat_list.Add(convertToString(cat.id));
                    }

                }

                string q;
                //if (cat_id.Last() == '0')
                if (min > max) // wrong case
                {
                    // reset to default and ignore
                    min = 0;
                    max = 0;
                }
                if ((min > 0) || (max > 0))
                {

                    if (cat_id.EndsWith(".0"))
                    {
                        string tmp = cat_id.Substring(0, cat_id.Length - 2);
                        //q = "{'active':1, 'cat_id.cat_id':/^" + tmp + "./}}";
                        q = "{'active':1,'cat_id.cat_id':/^" + tmp + "./ , 'price.list':{ $lte: " + max + ", $gte: " + min + "  }}";
                    }
                    else
                    {
                        //q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                        q = "{'active':1, cat_id:{cat_id:'" + cat_id + "'}, 'price.list':{ $lte: " + max + ", $gte: " + min + " }}";
                    }
                }
                else
                {


                    if (price_range == ">10000")
                    {
                        min_price = 10000;
                        max_price = 99999999;
                    }
                    else
                    {
                        string[] price_arr;
                        price_arr = price_range.Split('-');

                        if (price_arr.Length > 0)
                        {
                            min_price = convertToInt(price_arr[0]);
                            max_price = convertToInt(price_arr[1]);
                        }
                    }

                    q = "";

                    if (menu_type == "ReadyToShip")
                    {
                        if (cat_id_length == 2)
                        {
                            q = "{'active' : 1, 'Ready_to_ship' : '1', 'price.min_qty' : 1001, 'price.max_qty': 2000, 'price.list' :{ $gte: " + min_price + ", $lte: " + max_price + " } , 'cat_id.cat_id':{$in:[";
                        }
                        else if (cat_id_length == 3)
                        {
                            q = "{'active' : 1, 'Ready_to_ship' : '1', 'price.min_qty' : 1001, 'price.max_qty': 2000, 'price.list' :{ $gte: " + min_price + ", $lte: " + max_price + " }}";
                        }
                    }
                    else if (menu_type == "New")
                    {
                        int DateRange = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["DateRange"].ToString());
                        var EndDate = "'" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                        var StartDate = "'" + System.DateTime.UtcNow.AddDays(-DateRange).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                        if (cat_id_length == 2)
                        {
                            q = "{'active' : 1, 'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")} , 'price.min_qty' : 1001, 'price.max_qty': 2000, 'price.list' :{ $gte: " + min_price + ", $lte: " + max_price + " } , 'cat_id.cat_id':{$in:[";
                        }
                        else if (cat_id_length == 3)
                        {
                            q = "{'active' : 1, 'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")} , 'price.min_qty' : 1001, 'price.max_qty': 2000, 'price.list' :{ $gte: " + min_price + ", $lte: " + max_price + "}}";
                        }
                    }
                    else if (menu_type == "SortByPrice")
                    {
                        if (cat_id_length == 2)
                        {
                            q = "{'active' : 1, 'price.min_qty' : 1001, 'price.max_qty': 2000, 'price.list' :{ $gte: " + min_price + ", $lte: " + max_price + " } , 'cat_id.cat_id':{$in:[";
                        }
                        else if (cat_id_length == 3)
                        {
                            q = "{'active' : 1, 'price.min_qty' : 1001, 'price.max_qty': 2000, 'price.list' :{ $gte: " + min_price + ", $lte: " + max_price + " }}";
                        }
                    }

                    if (cat_id_arr.Length == 2)
                    {
                        q = generate_query(q, level3_cat_list);
                    }

                }

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = get_cat_short_include_fields();

                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);

                double min_quantity = 1001;

                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod = JsonConvert.DeserializeObject<product>(c.ToBsonDocument().ToString());
                    if (prod.price.Count() > 0)
                    {
                        List<price> prod_price = new List<price>();
                        prod_price = prod.price.FindAll(delegate(price m_in) { return m_in.min_qty == min_quantity; });
                        if (prod_price.Count() > 0)
                        {
                            if (convertToInt(prod_price[0].list) != 0)
                            {
                                if (convertToInt(prod_price[0].list) >= min_price && convertToInt(prod_price[0].list) <= max_price)
                                {
                                    if (c.ToBsonDocument()["have_child"] > 0)
                                    {
                                        c.ToBsonDocument()["stock"] = -9999;// hardcoded -negatve value to show products with children. system is prone to a situation of we get to -9999 stock - very unlikely
                                    }
                                    else
                                    {
                                        c.ToBsonDocument()["stock"] = c.ToBsonDocument()["stock"].ToInt32();
                                    }
                                    list.Add(c.ToBsonDocument().ToString());
                                }
                            }
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list.ToArray();

        }

        public MongoCursor get_category_data(string query)
        {

            try
            {
                dal.DataAccess dal = new DataAccess();

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(query);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "Name", "id", "ParentId" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
                return cursor;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string generate_query(string q, List<String> con_list)
        {

            string gen_q = q;
            foreach (var c in con_list)
            {
                if (gen_q == q)
                {
                    gen_q = gen_q + "'" + c + "'";
                }
                else
                {
                    gen_q = gen_q + ",'" + c + "'";
                }
            }
            gen_q = gen_q + "]}}";

            return gen_q;

        }

        public string[] get_breadcrumb(string type, string value)
        {
            string[] new_list = new string[] { "" };
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string[] include_fields;
                string[] exclude_fields = new string[] { "_id" };

                string l1, l2, qs;
                string[] s;
                List<string> distinct_cat = new List<string>();
                if (type == "")
                {
                    type = "cat";
                }
                type = type.ToLower();
                switch (type)
                {
                    case "cat":
                        // get parent cats also
                        s = value.Split('.');
                        switch (s.Length)
                        {
                            case 3:
                                l1 = s[0] + ".0";
                                l2 = s[0] + "." + s[1] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(l2);
                                distinct_cat.Add(value);
                                break;

                            case 2:
                                l1 = s[0] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(value);
                                break;
                            //case 1:
                            default:
                                break;
                        }

                        //distinct_cat = new List<string>(distinct_cat.Distinct());
                        qs = "{'id':{$in:[";
                        foreach (var c in distinct_cat)
                        {
                            if (qs == "{'id':{$in:[")
                            {
                                qs = qs + "'" + c + "'";
                            }
                            else
                            {
                                qs = qs + ",'" + c + "'";
                            }
                        }
                        qs = qs + "]}}";
                        include_fields = new string[] { "id", "Name", "description" };
                        exclude_fields = new string[] { "_id" };
                        new_list = mongo_query(qs, "category", include_fields, exclude_fields);
                        break;

                    case "prod":

                        // TODO - get parent cat and brand
                        // TODO - feed parent cat thru below loop
                        // concat brand at the end of the list

                        // get parent cats also

                        s = value.Split('.');
                        switch (s.Length)
                        {
                            case 3:
                                l1 = s[0] + ".0";
                                l2 = s[0] + "." + s[1] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(l2);
                                distinct_cat.Add(value);
                                break;

                            case 2:
                                l1 = s[0] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(value);
                                break;
                            //case 1:
                            default:
                                break;
                        }

                        //distinct_cat = new List<string>(distinct_cat.Distinct());
                        qs = "{'id':{$in:[";
                        foreach (var c in distinct_cat)
                        {
                            if (qs == "{'id':{$in:[")
                            {
                                qs = qs + "'" + c + "'";
                            }
                            else
                            {
                                qs = qs + ",'" + c + "'";
                            }
                        }
                        qs = qs + "]}}";
                        include_fields = new string[] { "id", "Name", "description" };
                        exclude_fields = new string[] { "_id" };
                        new_list = mongo_query(qs, "category", include_fields, exclude_fields);
                        break;


                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return new_list;

        }
        public string[] get_categories_by_brand(string brand)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'brand':'" + brand + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);

                List<String> list = new List<string>();
                foreach (var c in cursor)
                {

                    //list.Add(c.ToBsonDocument().ToString());
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list.Add(d["cat_id"].ToString());
                    }
                }
                List<string> distinct_cat = new List<string>(list.Distinct());//.ToArray(); // get disctinct

                // get parent cats also
                string l1, l2;
                string[] s;
                //var cat1 = distinct_cat;
                foreach (var c1 in distinct_cat.ToList())
                {
                    s = c1.Split('.');
                    switch (s.Length)
                    {
                        case 3:
                            l1 = s[0] + ".0";
                            l2 = s[0] + "." + s[1];
                            distinct_cat.Add(l1);
                            distinct_cat.Add(l2);
                            break;

                        case 2:
                            l1 = s[0] + ".0";
                            distinct_cat.Add(l1);
                            break;
                        //case 1:
                        default:
                            break;
                    }
                }
                distinct_cat = new List<string>(distinct_cat.Distinct());
                //{"id":{$in: ["1.1", "1.1.1"]}}
                string qs = "{'id':{$in:[";
                foreach (var c in distinct_cat)
                {
                    qs = qs + "'" + c + "'";
                }
                qs = qs + "]}}";
                include_fields = new string[] { "id", "Name", "description" };
                exclude_fields = new string[] { "_id" };
                var new_list = mongo_query(qs, "category", include_fields, exclude_fields);
                // now get cat names by id
                return new_list;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public string[] get_prod_list_by_brand(string brand)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'brand':'" + brand + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = get_cat_short_include_fields(); //new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }
        public string[] get_premium_prod_list(int premium_threshhold, int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{ 'active' : 1, 'price.list':{$lte: " + max + ", $gte:" + premium_threshhold + "}}";
            }
            else
            {
                q = "{ 'active' : 1, 'price.list':{$gte:" + premium_threshhold + "}}";
            }
            //q = "{ 'active' : 1, 'price.list':{$gte:" + premium_threshhold + "}}";

            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public string[] get_newarrivals_prod_list(int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();

            int DateRange = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["DateRange"].ToString());
            var EndDate = "'" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
            var StartDate = "'" + System.DateTime.UtcNow.AddDays(-DateRange).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{'active' : 1, 'price.list':{ $lte: " + max + ", $gte: " + min + "  },  'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")}}";

            }
            else
            {
                q = "{'active' : 1, 'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")}}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public string[] get_search_prod_list(string s, int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //string q = "{'brand':'" + brand + "'}}";
            //string q = "{ 'active' : 1, 'price.list':{$gt:'" + premium_threshhold + "'}}";
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{ 'active' : 1, 'price.list':{ $lte: " + max + ", $gte: " + min + "  }, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
            }
            else
            {
                q = "{ 'active' : 1, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }
        public string[] get_search_prod_list(string s, string context, int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                if (context != "0")
                {

                    q = "{ 'active' : 1, 'price.list':{ $lte: " + max + ", $gte: " + min + "  }, 'cat_id.cat_id':/^" + context.Substring(0, context.Length - 1) + "/, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
                else
                {
                    q = "{ 'active' : 1,'price.list':{ $lte: " + max + ", $gte: " + min + "  },  'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
            }
            else
            {

                if (context != "0")
                {

                    q = "{ 'active' : 1, 'cat_id.cat_id':/^" + context.Substring(0, context.Length - 1) + "/, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
                else
                {
                    q = "{ 'active' : 1, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public List<filter> get_search_prod_list_filters(string s)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }


        public List<filter> get_express_items_filters()
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'Express':'1'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }


        public string[] get_express_prod_list(int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{ 'active' : 1, 'Express':'1', 'price.list':{ $lte: " + max + ", $gte: " + min + "  } }";
            }
            else
            {
                q = "{ 'active' : 1, 'Express':'1'}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public string[] get_prod_list_by_brand(string brand, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'brand':'" + brand + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls","feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
        }


        public string[] get_prod_details(string product_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'active':1, id:'" + product_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "parent_cat_id", "sku", "point", "Name", "description", "shortdesc", "feature", "image_urls", "video_urls", "image", "Express", "stock", "block_number" };
            string[] exclude_fields = new string[] { "_id" };
            //return mongo_query(q, "product", include_fields, exclude_fields);
            List<string> list = new List<string>();
            List<string> child_list = new List<string>();
            list = mongo_query_list(q, "product", include_fields, exclude_fields);
            child_list = get_child_products(product_id);
            if (child_list.Count > 0)
            {
                foreach (string el in child_list)
                {
                    list.Add(el);
                }
            }
            return list.ToArray();
        }

        public string[] get_shipping_details(string cat_id)
        {
            // evoke mongodb connection method in dal

            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = "{'active':1, id:'" + cat_id + "'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "min_ship", "max_ship", "Shipping" };
                string[] exclude_fields = new string[] { "_id" };
                return mongo_query(q, "category", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //public prod get_prod_details_for_cart(string product_id)
        //{
        //    // evoke mongodb connection method in dal

        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{id:'" + product_id + "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "price", "sku", "size" };
        //    string[] exclude_fields = new string[] { "_id" };
        //    MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
        //    prod p = new prod();
        //    foreach (var c in cursor)
        //    {
        //        p.id = c.ToBsonDocument()["id"].ToString();
        //        p.ch
        //        var ch = get_child_products(p.id);
        //        p.sku = c.ToBsonDocument()["sku"].ToString();
        //        p.price = JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString());
        //    }


        //    return list.ToArray();
        //}

        //public string[] get_child_products(string product_id)
        //{
        //    // evoke mongodb connection method in dal
        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{parent_prod:'" + product_id + "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "id", "size", "sku","stock" };
        //    string[] exclude_fields = new string[] { "_id" };
        //    string[] ascending_sort_fields = { "id" };
        //    string[] descending_sort_fields = { };
        //    return mongo_query(q, "product", include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
        //}
        public List<product> get_prod_list_by_cat(string cat_id)
        {
            var products = new List<product>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{ cat : {$elemMatch : {id:'51bb3c22782b4c22745a5100'}}}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                foreach (var c in cursor)
                {
                    product prod = new product();
                    //prod.Id = c.ToBsonDocument()["_id"].ToString();
                    //prod.prod_name = c.ToBsonDocument()["prod_name"].ToString();
                    prod.sku = c.ToBsonDocument()["sku"].ToString();
                    prod.description = c.ToBsonDocument()["description"].ToString();
                    //prod.price = JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString());
                    prod.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    prod.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    prod.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["video_urls"].ToString());
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;

        }
        // this method returns validated cart by checking stock, active flag and price
        public user_cart validate_cart(string email_id)
        {
            try
            {
                List<cart_item> to_be_deleted = new List<cart_item>();
                user_cart uc = get_cart(email_id);
                foreach (cart_item c in uc.cart_items)
                {
                    // get product info for the cart item
                    product p = get_product_details(c.id);
                    if (p.active == 0)
                    {
                        //delete item
                        //uc.cart_items.Remove(c);
                        to_be_deleted.Add(c);
                    }
                    else
                    {
                        if (c.selected_size == "ONESIZE")
                        {
                            //product p = get_product_details(c.id);
                            //if (p.stock <= 0)
                            //{
                            //    // delete item
                            //    //uc.cart_items.Remove(c);
                            //    to_be_deleted.Add(c);
                            //}
                            //else
                            //{
                            //    //if (c.quantity > p.stock)
                            //    //{
                            //    //    c.quantity = (int)p.stock; // set quantity = stock
                            //    //}
                            //    //c.mrp = p.price.mrp;
                            //    //c.final_offer = p.price.final_offer;
                            //    //c.discount = p.price.discount;
                            //    if (p.Express == "1")
                            //    {
                            //        c.express = true;
                            //    }
                            //    else { c.express = false; }
                            //}

                        }
                        else
                        {
                            product p1 = get_product_details(c.cart_item_id);

                            //product p = get_product_details(c.id);
                            //if (p1.stock <= 0)
                            //{
                            //    // delete item
                            //    //uc.cart_items.Remove(c);
                            //    to_be_deleted.Add(c);
                            //}
                            //else
                            //{
                            //    if (c.quantity > p1.stock)
                            //    {
                            //        c.quantity = (int)p1.stock; // set quantity = stock
                            //    }
                            //    if (p1.Express == "1")
                            //    {
                            //        c.express = true;
                            //    }
                            //    else { c.express = false; }
                            //}


                        }

                    }

                }
                foreach (cart_item c in to_be_deleted)
                {
                    uc.cart_items.Remove(c);
                }
                write_cart(uc);
                return uc;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }

        private product get_product_details(string id)
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();

                //MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                string q = "{sku:'" + id + "'}";// qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "id", "parent_prod", "sku", "stock", "active", "price", "express" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                foreach (var c in cursor)
                {

                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.active = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
                    //prod.stock = check_field(c.ToBsonDocument(), "stock") ? convertToInt(c.ToBsonDocument()["stock"]) : 0;
                    //prod.price = check_field(c.ToBsonDocument(), "price") ? JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString()) : null;
                    prod.Express = check_field(c.ToBsonDocument(), "Express") ? convertToString(c.ToBsonDocument()["Express"]) : null;
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products[0];
        }

        private user_cart get_cart(string email_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{email_id:'" + email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);
                user_cart uc = new user_cart();
                foreach (var c in cursor)
                {
                    //uc = (user_cart) c.ToBsonDocument();
                    uc.email_id = c.ToBsonDocument()["email_id"].ToString();

                    foreach (BsonDocument b in c.ToBsonDocument()["cart_items"].AsBsonArray)
                    {
                        uc.cart_items.Add(
                         new cart_item()
                         {
                             id = b.ToBsonDocument()["_id"].ToString(),
                             cart_item_id = b.ToBsonDocument()["cart_item_id"].ToString(),
                             brand = b.ToBsonDocument()["brand"].ToString(),
                             discount = convertToDouble(b.ToBsonDocument()["discount"].ToString()),
                             express = b.ToBsonDocument()["express"].ToString() == "0" ? false : true,
                             final_offer = convertToDouble(b.ToBsonDocument()["final_offer"].ToString()),
                             mrp = convertToDouble(b.ToBsonDocument()["mrp"].ToString()),
                             quantity = convertToInt(b.ToBsonDocument()["quantity"].ToString()),
                             //sizes = JsonConvert.DeserializeObject<List<size>>(b.ToBsonDocument()["sizes"].ToString())
                             selected_size = convertToString(b.ToBsonDocument()["selected_size"].ToString())
                         }
                         );
                    }

                }

                return uc;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }
        }

        public List<cart_item> get_cart_by_email_for_order(string email_id)
        {
            var cart_items = new List<cart_item>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'email_id':'" + email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                //string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);


                foreach (var c in cursor)
                {
                    //cart_item cart = new cart_item();
                    cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

                    for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++)
                    {
                        cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
                    }

                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return cart_items;
        }

        public List<cart_item> get_cart_by_email(string email_id)
        {
            var cart_items = new List<cart_item>();

            var modified_cart_items = new List<cart_item>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'email_id':'" + email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                //string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);


                foreach (var c in cursor)
                {
                    //cart_item cart = new cart_item();
                    cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

                    for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++)
                    {
                        cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
                    }

                    for (int j = 0; j < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); j++)
                    {
                        int cur_prod_stock = get_prod_stock_qty(cart_items[j].cart_item_id.ToString());

                        string Product_ID = "";
                        var Prod_Id = cart_items[j].id.ToString().Split('.');
                        Product_ID = Prod_Id[0].ToString();

                        if (chk_prod_active(Product_ID) == 1)
                        {
                            if (cur_prod_stock > 0)
                            {
                                cart_item ci = new cart_item();
                                price op = new price();
                                op = get_prod_price(cart_items[j].id.ToString());
                                if (cart_items[j].quantity > cur_prod_stock)
                                {
                                    cart_items[j].quantity = cur_prod_stock;
                                }
                                cart_items[j].mrp = op.mrp;
                                cart_items[j].discount = op.discount;
                                cart_items[j].final_offer = op.final_offer;
                                ci = cart_items[j];
                                modified_cart_items.Add(ci);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return modified_cart_items;
        }


        public string get_bulk_cart_by_email(string email_id)
        {


            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'email_id':'" + email_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);

            string cart_data = "";


            foreach (var c in cursor)
            {
                cart_data = check_field(c.ToBsonDocument(), "cart_items") ? convertToString(c.ToBsonDocument()["cart_items"]) : null;
            }

            return cart_data;
        }

        public int chk_prod_active(string Product_ID)
        {

            int IsActive = 0;

            dal.DataAccess dal = new DataAccess();
            string q = "{id:'" + Product_ID + "'}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "active" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
            foreach (var c in cursor)
            {
                IsActive = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
            }
            return IsActive;
        }
        public int get_prod_stock_qty(string Product_sku)
        {
            try
            {
                // evoke mongodb connection method in dal

                int Quantity = 0;

                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{sku:'" + Product_sku + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    //prod.stock = check_field(c.ToBsonDocument(), "stock") ? convertToInt(c.ToBsonDocument()["stock"]) : 0;
                    products.Add(prod);
                }

                Quantity = convertToInt(products[0].stock.ToString());

                return Quantity;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        public price get_prod_price(string Product_id)
        {
            price prod_price = new price();

            try
            {
                // evoke mongodb connection method in dal


                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    //prod.price = JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString());
                    products.Add(prod);
                }

                //prod_price = products[0].price;

                return prod_price;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return prod_price;
            }
        }

        //Added By Hassan To Get The Wish List
        public List<cart_item> get_wishlist_by_email(string email_id)
        {
            var cart_items = new List<cart_item>();

            var modified_cart_items = new List<cart_item>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'email_id':'" + email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                //string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("wishlist", queryDoc);


                foreach (var c in cursor)
                {
                    //cart_item cart = new cart_item();
                    cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

                    for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++)
                    {
                        cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
                    }

                    for (int j = 0; j < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); j++)
                    {
                        int cur_prod_stock = get_prod_stock_qty(cart_items[j].cart_item_id.ToString());

                        string Product_ID = "";
                        var Prod_Id = cart_items[j].id.ToString().Split('.');
                        Product_ID = Prod_Id[0].ToString();

                        if (chk_prod_active(Product_ID) == 1)
                        {

                            if (cur_prod_stock > 0)
                            {
                                cart_item ci = new cart_item();
                                price op = new price();
                                op = get_prod_price(cart_items[j].id.ToString());
                                if (cart_items[j].quantity > cur_prod_stock)
                                {
                                    cart_items[j].quantity = cur_prod_stock;
                                }
                                cart_items[j].mrp = op.mrp;
                                cart_items[j].discount = op.discount;
                                cart_items[j].final_offer = op.final_offer;
                                ci = cart_items[j];
                                modified_cart_items.Add(ci);
                            }
                        }
                    }
                }
                //return mongo_query_singleton (q, "cart", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return modified_cart_items;
        }


        //Added By Dipanjan To Get The Product By Brand
        public string[] get_prod_by_brand(string brand_name)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'active':1,'brand':/^" + brand_name + "$/i}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = get_cat_short_include_fields(); //new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }


        //public List<cart_item> get_cart_by_email(string email_id)
        //{
        //    // evoke mongodb connection method in dal
        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{'email_id':'" + email_id+ "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "email_id", "cart_items" };
        //    //string[] exclude_fields = new string[] { "_id" };
        //    MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);

        //    var cart_items = new List<cart_item>();
        //    foreach (var c in cursor)
        //    {
        //        //cart_item cart = new cart_item();
        //        cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

        //        for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++ )
        //        {
        //            cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
        //        }
        //        //cart.sizes = JsonConvert.DeserializeObject<List<size>>(c.ToBsonDocument()["sizes"].ToString());

        //        //cart.id = c.ToBsonDocument()["id"].ToString();
        //        //cart.brand = c.ToBsonDocument()["brand"].ToString();
        //        //cart.cart_item_id = c.ToBsonDocument()["cart_item_id"].ToString();
        //        //cart.image = c.ToBsonDocument()["image"].ToString();
        //        //cart.sku = c.ToBsonDocument()["sku"].ToString();
        //        //cart.name = c.ToBsonDocument()["name"].ToString();
        //        //cart.mrp = convertToDouble(c.ToBsonDocument()["name"].ToString());
        //        //cart.final_offer = convertToDouble(c.ToBsonDocument()["final_offer"].ToString());
        //        //cart.discount = convertToDouble(c.ToBsonDocument()["discount"].ToString());
        //        //cart.express = c.ToBsonDocument()["discount"].ToBoolean();
        //        //cart.sizes = JsonConvert.DeserializeObject<List<size>>(c.ToBsonDocument()["sizes"].ToString());
        //        //cart_items.Add(cart);
        //    }
        //    //return mongo_query_singleton (q, "cart", include_fields, exclude_fields);
        //    return cart_items;
        //}

        // mysql

        public List<user_shipping> get_user_shipping_info(string user_id)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<user_shipping> user_shipping_list = new List<user_shipping>();
            try
            {
                dtResult = da.ExecuteDataTable("get_shipping_by_user"
                                , da.Parameter("_user_id", user_id)
                                );




                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        user_shipping_list.Add(
                        new user_shipping
                        {
                            id = convertToInt(dr["id"].ToString()),
                            address = convertToString(dr["address"]),
                            city = convertToString(dr["city"]),
                            state = convertToString(dr["state"]),
                            pincode = convertToString(dr["pincode"]),
                            shipping_name = convertToString(dr["shipping_name"]),
                            mobile_number = convertToString(dr["mobile_number"]),
                            name = convertToString(dr["name"])
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return user_shipping_list;

        }
        public List<point_details> get_user_points_credit(string user_id)
        {
            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<point_details> point_details_credit = new List<point_details>();
            try
            {
                dtResult = da.ExecuteDataTable("get_user_point_credit"
                                , da.Parameter("_user_id", user_id)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        point_details_credit.Add(
                        new point_details
                        {
                            user_id = convertToString(dr["user_id"]),
                            txn_amount = convertToDouble(dr["txn_amount"]),
                            txn_ts = convertToDate(dr["txn_ts"]),
                            txn_comment = convertToString(dr["txn_comment"]),
                            txn_type = "c"
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return point_details_credit;
        }
        public List<point_details> get_user_points_debit(string user_id)
        {
            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<point_details> point_details_debit = new List<point_details>();
            try
            {
                dtResult = da.ExecuteDataTable("get_user_point_debit"
                                , da.Parameter("_user_id", user_id)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        point_details_debit.Add(
                        new point_details
                        {
                            user_id = convertToString(dr["user_id"]),
                            txn_amount = convertToDouble(dr["txn_amount"]),
                            txn_ts = convertToDate(dr["txn_ts"]),
                            txn_comment = convertToString(dr["txn_comment"]),
                            txn_type = "d"
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return point_details_debit;
        }


        public long get_points_against_user(string company, string email_id)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();

            long Points = 0;
            try
            {

                dtResult = da.ExecuteDataTable("get_user_points"
                               , da.Parameter("_company", company)
                               , da.Parameter("_email_id", email_id)
                               );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        Points = convertToLong(dtResult.Rows[i]["PointsBalance"]);
                    }

                }
                return Points;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

        }


        public List<order> get_user_order_details(string user_id)
        {
            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<order> order_details = new List<order>();
            try
            {
                dtResult = da.ExecuteDataTable("get_user_order_details"
                                , da.Parameter("_user_id", user_id)
                                , da.Parameter("_flag", 0)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        order_details.Add(
                        new order
                        {
                            user_id = convertToString(dr["user_id"]),
                            order_id = convertToLong(dr["order_id"]),
                            total_amount = convertToDecimal(dr["total_amount"]),
                            paid_amount = convertToDecimal(dr["paid_amount"]),
                            create_ts = convertToDate(dr["create_ts"]),
                            payment_info = Convert.ToString(dr["payment_info"]),
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return order_details;
        }

        public List<my_order> get_my_order_details(string user_id)
        {
            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<my_order> order_details = new List<my_order>();
            try
            {
                dtResult = da.ExecuteDataTable("get_salesperson_by_order_id"
                                , da.Parameter("_user_id", user_id)

                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        order_details.Add(
                        new my_order
                        {
                            order_id = convertToInt(dr["order_id"]),
                            billing_name = convertToString(dr["billing_name"]),
                            sales_person_name = convertToString(dr["sales_person_name"]),
                            total_amount = convertToDouble(dr["total_amount"]),
                            payment_info = convertToString(dr["payment_info"])


                            //user_id = convertToString(dr["user_id"]),
                            //order_id = convertToLong(dr["order_id"]),
                            //total_amount = convertToDecimal(dr["total_amount"]),
                            //paid_amount = convertToDecimal(dr["paid_amount"]),
                            //create_ts = convertToDate(dr["create_ts"]),
                            //payment_info = Convert.ToString(dr["payment_info"]),
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return order_details;
        }




        public int add_express_shipping(int order_id, int express_shipping_amount)
        {

            DataAccess da = new DataAccess();
            int o_id = da.ExecuteSP("add_express_shipping",
                            da.Parameter("_order_id", order_id),
                            da.Parameter("_express_shipping", express_shipping_amount)
                          );
            return o_id;
        }


        public int write_ship_data(user_shipping s)
        {
            try
            {
                // 1. Get Cart info from MongoDB
                List<cart_item> cart_items = new List<cart_item>();
                //cart_items = get_cart_by_email(s.user_id);
                cart_items = get_cart_by_email_for_order(s.user_id);

                // 2. Save Shipping Data
                DataAccess da = new DataAccess();
                DataTable dt_ship_info = da.ExecuteDataTable("write_user_shipping",
                                da.Parameter("_id", s.id),
                                da.Parameter("_user_id", s.user_id),
                                da.Parameter("_shipping_name", s.shipping_name),
                                da.Parameter("_pincode", s.pincode),
                                da.Parameter("_address", s.address),
                                da.Parameter("_state", s.state),
                                da.Parameter("_city", s.city),
                                da.Parameter("_mobile_number", s.mobile_number),
                                da.Parameter("_name", s.name)
                              );
                s.id = convertToInt(dt_ship_info.Rows[0]["shipping_info_id"].ToString());
                // 3. Save Cart details in mysql - this is when cart becomes close to order
                cart_data c = new cart_data()
                {
                    cart_id = Guid.NewGuid().ToString(),
                    cart_data_json = Newtonsoft.Json.JsonConvert.SerializeObject(cart_items),
                    user_id = s.user_id
                };


                int retval = da.ExecuteSP("cart_data_write",
                             da.Parameter("_cart_data_id", c.cart_id),
                             da.Parameter("_cart_data", c.cart_data_json),
                             da.Parameter("_user_id", c.user_id)
                             );
                // 3a. Get Shipping information
                double shipping_charge = 0;
                double min_order_for_free_shipping = 0;
                DataTable dt_ship_charge = da.ExecuteDataTable("get_shipping_charges_by_user",
                                           da.Parameter("_email_id", c.user_id)
                                           );
                if ((dt_ship_charge != null) && (dt_ship_charge.Rows.Count > 0))
                {
                    shipping_charge = convertToDouble(dt_ship_charge.Rows[0]["shipping_charge"].ToString());
                    min_order_for_free_shipping = convertToDouble(dt_ship_charge.Rows[0]["min_order_for_free_shipping"].ToString());
                }
                else
                {
                    shipping_charge = 0;
                    min_order_for_free_shipping = 0;
                }

                // 4. Save in Order Table 
                // get cart total
                double cart_total = 0;
                foreach (cart_item cart in cart_items)
                {
                    cart_total = cart_total + cart.final_offer * cart.quantity;
                }
                if (cart_total < min_order_for_free_shipping)
                {
                    cart_total = cart_total + shipping_charge;
                }

                // create order object
                user_order o = new user_order()
                {
                    order_id = 0, // first time order getting generated
                    order_display_id = Guid.NewGuid().ToString(),
                    store = s.store,
                    cart_id = c.cart_id,
                    shipping_info_id = s.id,
                    user_id = c.user_id,
                    discount_code = "default",
                    total_amount = cart_total,
                    points = 0,
                    paid_amount = 0,
                    status = 0, // order booked
                    payment_info = "",
                    rejection_reason = "",
                    create_ts = DateTime.Now,
                    modify_ts = DateTime.Now
                };


                DataTable dt_order_info = da.ExecuteDataTable("order_Insert",
                             da.Parameter("_order_id", o.order_id),
                             da.Parameter("_cart_id", o.cart_id),
                             da.Parameter("_user_id", o.user_id),
                             da.Parameter("_discount_code", o.discount_code),
                             da.Parameter("_total_amount", o.total_amount),
                             da.Parameter("_points", o.points),
                             da.Parameter("_paid_amount", o.paid_amount),
                             da.Parameter("_status", o.status),
                             da.Parameter("_payment_info", o.payment_info),
                             da.Parameter("_rejection_reason", o.rejection_reason),
                             da.Parameter("_create_ts", o.create_ts),
                             da.Parameter("_modify_ts", o.modify_ts),
                             da.Parameter("_order_display_id", o.order_display_id),
                             da.Parameter("_store", o.store),
                             da.Parameter("_shipping_info_id", o.shipping_info_id)
                             );
                o.order_id = convertToInt(dt_order_info.Rows[0]["order_id"].ToString());
                return o.order_id;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        public int write_bulk_ship_data(bulk_shipping s)
        {
            try
            {
                string cart_items = "";

                // 1. Get Cart info from MongoDB
                //List<cart_item> cart_items = new List<cart_item>();
                //cart_items = get_cart_by_email(s.user_id);
                // cart_items = get_cart_by_email_for_order(s.user_id);
                cart_items = get_bulk_cart_by_email(s.user_id.ToString());

                var cartdata = JsonConvert.DeserializeObject<List<cart_items>>(cart_items.ToString());

                // 2. Save Shipping Data
                DataAccess da = new DataAccess();
                DataTable dt_ship_info = da.ExecuteDataTable("write_user_bulk_shipping",
                                          da.Parameter("_id", s.id),
                                          da.Parameter("_user_id", s.user_id),
                                          da.Parameter("_billing_name", s.billing_name),
                                          da.Parameter("_address", s.address),
                                          da.Parameter("_locality", s.locality),
                                          da.Parameter("_city", s.city),
                                          da.Parameter("_company", s.company),
                                          da.Parameter("_pin", s.pin),
                                          da.Parameter("_state", s.state),
                                          da.Parameter("_mobile_no", s.mobile_no),
                                          da.Parameter("_sales_person_name", s.sales_person_name),
                                          da.Parameter("_sales_person_mobile", s.sales_person_mobile));

                s.id = convertToInt(dt_ship_info.Rows[0]["shipping_info_id"].ToString());
                // 3. Save Cart details in mysql - this is when cart becomes close to order
                cart_data c = new cart_data()
                {
                    cart_id = Guid.NewGuid().ToString(),
                    // cart_data_json = Newtonsoft.Json.JsonConvert.SerializeObject(cart_items),
                    cart_data_json = cart_items,
                    user_id = s.user_id
                };


                int retval = da.ExecuteSP("cart_data_write",
                             da.Parameter("_cart_data_id", c.cart_id),
                             da.Parameter("_cart_data", c.cart_data_json),
                             da.Parameter("_user_id", c.user_id)
                             );
                // 3a. Get Shipping information
                //double shipping_charge = 0;
                //double min_order_for_free_shipping = 0;
                //DataTable dt_ship_charge = da.ExecuteDataTable("get_shipping_charges_by_user",
                //                           da.Parameter("_email_id", c.user_id)
                //                           );
                //if ((dt_ship_charge != null) && (dt_ship_charge.Rows.Count > 0))
                //{
                //    shipping_charge = convertToDouble(dt_ship_charge.Rows[0]["shipping_charge"].ToString());
                //    min_order_for_free_shipping = convertToDouble(dt_ship_charge.Rows[0]["min_order_for_free_shipping"].ToString());
                //}
                //else
                //{
                //    shipping_charge = 0;
                //    min_order_for_free_shipping = 0;
                //}

                //// 4. Save in Order Table 
                //// get cart total
                //double cart_total = 0;
                //foreach (cart_item cart in cart_items)
                //{
                //    cart_total = cart_total + cart.final_offer * cart.quantity;
                //}
                //if (cart_total < min_order_for_free_shipping)
                //{
                //    cart_total = cart_total + shipping_charge;
                //}

                // create order object
                user_order o = new user_order()
                {
                    order_id = 0, // first time order getting generated
                    order_display_id = Guid.NewGuid().ToString(),
                    store = s.store,
                    cart_id = c.cart_id,
                    shipping_info_id = s.id,
                    user_id = c.user_id,
                    discount_code = "default",
                    total_amount = s.cart_total,
                    points = 0,
                    paid_amount = 0,
                    status = 0, // order booked
                    payment_info = "",
                    rejection_reason = "",
                    create_ts = DateTime.Now,
                    modify_ts = DateTime.Now,
                    shipping_amount = s.shipping_amount
                };


                DataTable dt_order_info = da.ExecuteDataTable("puma_order_insert",
                             da.Parameter("_order_id", o.order_id),
                             da.Parameter("_cart_id", o.cart_id),
                             da.Parameter("_user_id", o.user_id),
                             da.Parameter("_discount_code", o.discount_code),
                             da.Parameter("_total_amount", o.total_amount),
                             da.Parameter("_points", o.points),
                             da.Parameter("_paid_amount", o.paid_amount),
                             da.Parameter("_status", o.status),
                             da.Parameter("_payment_info", o.payment_info),
                             da.Parameter("_rejection_reason", o.rejection_reason),
                             da.Parameter("_create_ts", o.create_ts),
                             da.Parameter("_modify_ts", o.modify_ts),
                             da.Parameter("_order_display_id", o.order_display_id),
                             da.Parameter("_store", o.store),
                             da.Parameter("_shipping_info_id", o.shipping_info_id),
                             da.Parameter("_shipping_amount", o.shipping_amount));

                o.order_id = convertToInt(dt_order_info.Rows[0]["order_id"].ToString());

                var item_no = 0;

                if (cartdata != null)
                {
                    for (int i = 0; i < cartdata.Count(); i++)
                    {
                        for (int j = 0; j < cartdata[i].child_prod_detail.Count(); j++)
                        {


                            if (cartdata[i].child_prod_detail[j].ord_qty != "" && convertToInt(cartdata[i].child_prod_detail[j].ord_qty) > 0)
                            {
                                item_no = item_no + 1;

                                var total_ord_qty = convertToInt(cartdata[i].order_quantity);
                                var prod_price = cartdata[i].parent_prod_detail.price;
                                var prod_final_offer = 0.0;
                                var prod_final_discount = 0.0;
                                var prod_mrp = 0.0;

                                foreach (price item in prod_price)
                                {
                                    if (total_ord_qty == item.min_qty || total_ord_qty == item.max_qty || (total_ord_qty > item.min_qty && total_ord_qty < item.max_qty))
                                    {
                                        prod_final_offer = Math.Round(item.list);
                                        prod_final_discount = item.discount;
                                        prod_mrp = item.mrp;
                                    }
                                }

                                int outval = da.ExecuteSP("partial_order_insert",
                                    da.Parameter("_order_id", o.order_id),
                                    da.Parameter("_cart_id", o.cart_id),
                                    da.Parameter("_user_id", o.user_id),
                                    da.Parameter("_discount_code", o.discount_code),
                                    da.Parameter("_total_amount", o.total_amount),
                                    da.Parameter("_points", o.points),
                                    da.Parameter("_paid_amount", o.paid_amount),
                                    da.Parameter("_status", o.status),
                                    da.Parameter("_payment_info", o.payment_info),
                                    da.Parameter("_rejection_reason", o.rejection_reason),
                                    da.Parameter("_create_ts", o.create_ts),
                                    da.Parameter("_modify_ts", o.modify_ts),
                                    da.Parameter("_order_display_id", o.order_display_id),
                                    da.Parameter("_store", o.store),
                                    da.Parameter("_shipping_info_id", o.shipping_info_id),

                                    da.Parameter("_brand", cartdata[i].parent_prod_detail.Brand),
                                    da.Parameter("_cart_item_id", cartdata[i].child_prod_detail[j].sku),
                                    da.Parameter("_discount", prod_final_discount),
                                    da.Parameter("_express", 0),
                                    da.Parameter("_final_offer", prod_final_offer),
                                    da.Parameter("_id", cartdata[i].child_prod_detail[j].id),
                                    da.Parameter("_mrp", prod_mrp),
                                    da.Parameter("_name", cartdata[i].parent_prod_detail.Name),
                                    da.Parameter("_quantity", cartdata[i].child_prod_detail[j].ord_qty),
                                    da.Parameter("_selected_size", cartdata[i].child_prod_detail[j].size),
                                    da.Parameter("_sku", cartdata[i].child_prod_detail[j].sku),
                                    da.Parameter("_item_no", item_no),

                                    da.Parameter("_del_qty", 0),
                                    da.Parameter("_display_name", convertToString(cartdata[i].parent_prod_detail.image_urls[0].display_name)),
                                    da.Parameter("_link", convertToString(cartdata[i].parent_prod_detail.image_urls[0].link)),
                                    da.Parameter("_thumb_link", convertToString(cartdata[i].parent_prod_detail.image_urls[0].thumb_link)),
                                    da.Parameter("_zoom_link", convertToString(cartdata[i].parent_prod_detail.image_urls[0].zoom_link)),
                                    da.Parameter("_item_status", 0)

                                    );

                            }




                        }


                    }
                }

                return o.order_id;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }



        public List<cart_item> get_cart_list_data(List<cart_items> cart)
        {
            List<cart_items> lst = cart;
            List<cart_item> lst_cart = new List<cart_item>();

            for (int j = 0; j < lst.Count; j++)
            {

                foreach (child_cart_items ch in lst[j].child_prod_detail)
                {

                    if (convertToInt(ch.ord_qty) > 0)
                    {

                        cart_item ci = new cart_item();
                        bool express;
                        if (lst[j].parent_prod_detail.Express == "1")
                        {
                            express = true;
                        }
                        else
                        {
                            express = false;
                        }


                        ci.id = ch.id;
                        ci.sku = ch.sku;
                        ci.quantity = convertToInt(ch.ord_qty);
                        ci.name = lst[j].parent_prod_detail.Name;
                        ci.brand = lst[j].parent_prod_detail.Brand;
                        ci.mrp = convertToDouble(lst[j].parent_prod_detail.price[j].mrp);

                        var total_ord_qty = lst[j].order_quantity;
                        var prod_price = lst[j].parent_prod_detail.price;
                        var prod_final_offer = 0.0;
                        var prod_final_discount = 0.0;
                        foreach (price item in prod_price)
                        {

                            if (total_ord_qty == item.min_qty || total_ord_qty == item.max_qty || (total_ord_qty > item.min_qty && total_ord_qty < item.max_qty))
                            {
                                prod_final_offer = Math.Round(item.list);
                                prod_final_discount = item.discount;
                            }

                        }

                        //ci.final_offer = convertToDouble(lst[j].parent_prod_detail.price[j].final_offer);
                        ci.final_offer = prod_final_offer;
                        ci.express = express;
                        List<size> lst_size = new List<size>();
                        var x = new size();
                        lst_size.Add(x);
                        x.size_name = ch.size;
                        ci.sizes = lst_size;
                        ci.selected_size = "";
                        ci.cart_item_id = "";
                        //ci.discount = convertToDouble(lst[j].parent_prod_detail.price[j].final_discount);
                        ci.discount = prod_final_discount;
                        ci.image = lst[j].parent_prod_detail.image_urls[j];
                        lst_cart.Add(ci);
                    }

                }


            }
            return lst_cart;

        }


        #endregion



        #region Private Methods
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private float convertToFloat(object o)
        {
            try
            {
                if (o != null)
                {
                    return float.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != "") && (o.ToString() != "BsonNull"))
                {
                    return o.ToString();
                }
                else if (o.ToString() == "BsonNull")
                {
                    return "";
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }
        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string[] get_prod_short_include_fields()
        {
            string[] include_fields = new string[] { "id", "sku", "price", "cat_id", "parent_cat_id" };
            return include_fields;
        }
        private string[] get_cat_short_include_fields()
        {
            string[] include_fields = new string[] {    "id", "stock","brand", "price", 
                                                        "cat_id", "sku", "point", 
                                                        "Name", "description", 
                                                        "image_urls", "feature", 
                                                        "have_child", "Express","shipping_days" };
            return include_fields;
        }
        public List<string> get_child_products(string product_id)
        {
            List<string> list = new List<string>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{parent_prod:'" + product_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "id", "size", "sku", "stock", "block_number" };
                string[] exclude_fields = new string[] { "_id" };
                string[] ascending_sort_fields = { "id" };
                string[] descending_sort_fields = { };
                list = mongo_query_list(q, "product", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list;
        }
        //private List<childproduct> get_child_products(string product_id)
        //{
        //    // evoke mongodb connection method in dal
        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{parent_prod:'" + product_id + "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "id", "size", "sku", "stock" };
        //    string[] exclude_fields = new string[] { "_id" };
        //    MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

        //    var child_prod_list = new List<childproduct>();
        //    foreach (var c in cursor)
        //    {
        //        childproduct ch = new childproduct();
        //        ch.id = c.ToBsonDocument()["id"].ToString();
        //        ch.size = c.ToBsonDocument()["size"].ToString();
        //        ch.sku = c.ToBsonDocument()["sku"].ToString();
        //        ch.stock = convertToDouble( c.ToBsonDocument()["stock"].ToString());
        //        child_prod_list.Add(ch);
        //    }
        //    return child_prod_list;
        //}
        private string[] mongo_query(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            List<String> list = new List<string>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);

                //foreach (var c in cursor)
                //{

                //    list.Add(c.ToBsonDocument().ToString());
                //}
                bool l_prodlist_call = false;
                foreach (var c in cursor)
                {

                    for (int i = 0; i < c.ToBsonDocument().ElementCount; i++)
                    {
                        if (c.ToBsonDocument().ElementAt(i).Name == "have_child")
                        {
                            l_prodlist_call = true;
                            break;
                        }
                    }
                    if (l_prodlist_call)
                    {
                        if (c.ToBsonDocument()["have_child"] > 0)
                        {
                            c.ToBsonDocument()["stock"] = -9999;
                        }
                        else
                        {
                            c.ToBsonDocument()["stock"] = c.ToBsonDocument()["stock"].ToInt32();
                        }
                    }
                    list.Add(c.ToBsonDocument().ToString());
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list.ToArray();
        }




        private string mongo_query_singleton(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields, null, null);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {
                list.Add(c.ToBsonDocument().ToString());
            }
            return list[0];
        }
        private long mongo_query_count(string qs, string collection)
        {
            dal.DataAccess dal = new DataAccess();

            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qs);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "_id" };
            string[] exclude_fields = new string[] { "" };
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc);
            int count = 0;
            if (cursor != null)
            {
                foreach (var c in cursor)
                {
                    count++;
                }
                return count;
            }
            else
            {
                return 0;
            }
        }
        private string[] mongo_query(string qs, string collection, string[] include_fields, string[] exclude_fields, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();
        }
        private List<string> mongo_query_list(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {
                c.ToBsonDocument()["stock"] = c.ToBsonDocument()["stock"].ToString();

                list.Add(c.ToBsonDocument().ToString());
            }
            return list;
        }
        private List<string> mongo_query_list(string qs, string collection, string[] include_fields, string[] exclude_fields, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list;
        }
        public void del_cart(string email_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //BsonDocument bd_deals = dl.ToBsonDocument();
            var whereclause = "";
            whereclause = "{email_id:'" + email_id + "'}";
            dal.mongo_remove("cart", whereclause);
        }
        private void insert_cart(user_cart cart)
        {
            // evoke mongodb connection method in dal
            //cart._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cart = cart.ToBsonDocument();
            string result = dal.mongo_write("cart", bd_cart);
        }

        // Added By Hassan
        public void del_wishlist(string email_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //BsonDocument bd_deals = dl.ToBsonDocument();
            var whereclause = "";
            whereclause = "{email_id:'" + email_id + "'}";
            dal.mongo_remove("wishlist", whereclause);
        }
        // Added By Hassan
        private void insert_wishlist(user_cart cart)
        {
            // evoke mongodb connection method in dal
            //cart._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cart = cart.ToBsonDocument();
            string result = dal.mongo_write("wishlist", bd_cart);
        }

        // Added By Hassan
        public bool wishlist_exists(string email_id)
        {
            string q = "{email_id:'" + email_id + "'}}";
            if (mongo_query_count(q, "wishlist") > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public bool cart_exists(string email_id)
        {
            string q = "{email_id:'" + email_id + "'}}";
            if (mongo_query_count(q, "cart") > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region post methodsval
        public string write_cart(user_cart cart)
        {
            try
            {
                // evoke mongodb connection method in dal
                cart._id = ObjectId.GenerateNewId().ToString();
                string email = cart.email_id;
                dal.DataAccess dal = new DataAccess();
                if (cart_exists(email))
                {
                    // frst delete
                    del_cart(email);
                    // then insert
                    insert_cart(cart);

                }
                else
                {
                    // insert
                    insert_cart(cart);
                }
                return "success";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return ex.Message.ToString();
            }

        }


        public string write_bulk_cart(bulk_cart cart)
        {
            try
            {
                // evoke mongodb connection method in dal
                cart._id = ObjectId.GenerateNewId().ToString();
                string email = cart.email_id;
                dal.DataAccess dal = new DataAccess();
                if (cart_exists(email))
                {
                    // frst delete
                    del_cart(email);
                    // then insert
                    insert_bulk_cart(cart);

                }
                else
                {
                    // insert
                    insert_bulk_cart(cart);
                }
                return "success";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return ex.Message.ToString();
            }

        }

        private void insert_bulk_cart(bulk_cart cart)
        {
            // evoke mongodb connection method in dal
            //cart._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cart = cart.ToBsonDocument();
            string result = dal.mongo_write("cart", bd_cart);
        }

        // Added By Hassan To Write Wish List 
        public string write_wishlist(user_cart cart)
        {
            try
            {
                // evoke mongodb connection method in dal
                cart._id = ObjectId.GenerateNewId().ToString();
                string email = cart.email_id;
                dal.DataAccess dal = new DataAccess();
                if (wishlist_exists(email))
                {
                    // frst delete
                    del_wishlist(email);
                    // then insert
                    insert_wishlist(cart);

                }
                else
                {
                    // insert
                    insert_wishlist(cart);
                }
                return "success";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return ex.Message.ToString();
            }

        }

        public user_cart reprice_cart(user_cart cart)
        {
            try
            {
                // get cart
                //var  c = new user_cart();
                //c = get_cart(cart.email_id);


                dal.DataAccess dal = new DataAccess();
                string q = "{email_id:'" + cart.email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);
                user_cart uc = new user_cart();
                foreach (var c in cursor)
                {
                    //uc = (user_cart) c.ToBsonDocument();
                    uc.email_id = c.ToBsonDocument()["email_id"].ToString();

                    foreach (BsonDocument b in c.ToBsonDocument()["cart_items"].AsBsonArray)
                    {
                        uc.cart_items.Add(
                             new cart_item()
                             {
                                 cart_item_id = b.ToBsonDocument()["cart_item_id"].ToString(),
                                 brand = b.ToBsonDocument()["brand"].ToString(),
                                 discount = convertToDouble(b.ToBsonDocument()["discount"].ToString()),
                                 express = b.ToBsonDocument()["express"].ToString() == "0" ? false : true,
                                 final_offer = convertToDouble(b.ToBsonDocument()["final_offer"].ToString()),
                                 image = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<imgurl>(b.ToBsonDocument()["image"].AsBsonDocument),
                                 mrp = convertToDouble(b.ToBsonDocument()["mrp"].ToString()),
                                 quantity = convertToInt(b.ToBsonDocument()["quantity"].ToString()),
                                 selected_size = convertToString(b.ToBsonDocument()["selected_size"]),
                                 sku = b.ToBsonDocument()["sku"].ToString(),
                                 name = b.ToBsonDocument()["name"].ToString(),
                                 sizes = b.ToBsonDocument()["sizes"].ToString() == "BsonNull" ? null : MongoDB.Bson.Serialization.BsonSerializer.Deserialize<List<size>>(b.ToBsonDocument()["sizes"].ToJson()),
                             });
                    }

                }

                //                return uc;//



                //replace size and price node


                return null;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }
        public string insert_catagory(catagory cat)
        {
            // evoke mongodb connection method in dal
            cat.Id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cat = cat.ToBsonDocument();
            string result = dal.mongo_write("cat", bd_cat);

            return result;
        }

        public string[] post_prod_list_by_qs(string qs)
        {

            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();

        }

        public void prod_view_insert(prod_view pv)
        {
            pv._id = ObjectId.GenerateNewId().ToString();
            pv.view_ts = DateTime.Now;
            dal.DataAccess dal = new DataAccess();
            var result = dal.mongo_write("prod_view", pv.ToBsonDocument());
        }


        #endregion

    }
}
