﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ecomm.util.entities;
using ecomm.dal;
using System.Data;
using ecomm.util;


namespace ecomm.model.repository
{
    public class admin_repository
    {
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }

                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        
        #region ===============Company=====================
        public string CompanyEntry(COMPANY cf)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            try
            {

                int i = da.ExecuteSP("comp_insert", ref oc
                                , da.Parameter("strcompid", cf.id)
                                , da.Parameter("strname", cf.name)
                                , da.Parameter("strdisplayname", cf.displayname)
                                , da.Parameter("strcontactname", cf.contactname)
                                , da.Parameter("strphonenumber", cf.phonenumber)
                                , da.Parameter("straddress", cf.address)
                                , da.Parameter("stremailid", cf.emailid)
                                , da.Parameter("strlogo", cf.logo)
                                , da.Parameter("inrratio",cf.points_ratio)
                                , da.Parameter("dpoint_range_min", cf.point_range_min)
                                , da.Parameter("dpoint_range_max", cf.point_range_max)
                                , da.Parameter("sroot_url_1", cf.root_url_1)
                                , da.Parameter("sroot_url_2", cf.root_url_2)
                                , da.Parameter("dshipping_charge", cf.shipping_charge)
                                , da.Parameter("dmin_order_for_free_shipping", cf.min_order_for_free_shipping)
                                , da.Parameter("intsalestracking", cf.sales_tracking)
                                , da.Parameter("strtrackcycle", cf.track_cycle)
                                , da.Parameter("intvalidity", cf.validity)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {
                    return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }

        public string UserEntry(user_security us)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                int i = da.ExecuteSP("user_inser_update", ref oc
                                , da.Parameter("_Id", us.Id)
                                , da.Parameter("_user_name", us.user_name)
                                , da.Parameter("_company_id", us.company_id)
                                , da.Parameter("_password", us.password)
                                , da.Parameter("_user_type", us.user_type)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed";
            }



        }
        public List<COMPANY> PopulateCompany()
        {

            DataAccess da = new DataAccess();

            List<COMPANY> lcomp = new List<COMPANY>();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("company_details"
                                , da.Parameter("_comp_id", 0)
                                , da.Parameter("_entry_mode", "N")
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        COMPANY comp = new COMPANY();
                        comp.id = Convert.ToInt32(dtResult.Rows[i]["COMPANY_ID"].ToString());
                        comp.name = dtResult.Rows[i]["COMPANY_NAME"].ToString();
                        lcomp.Add(comp);
                    }
                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return lcomp;
        }

        public List<COMPANY> get_comp_details(COMPANY c)
        {

            DataAccess da = new DataAccess();

            List<COMPANY> lcomp = new List<COMPANY>();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("company_details"
                                 , da.Parameter("_comp_id", c.id)
                                 , da.Parameter("_entry_mode", "E")
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        COMPANY comp = new COMPANY();
                        comp.id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                        comp.name = dtResult.Rows[i]["name"].ToString();
                        comp.displayname = dtResult.Rows[i]["displayname"].ToString();
                        comp.contactname = dtResult.Rows[i]["contactname"].ToString();
                        comp.phonenumber = dtResult.Rows[i]["phonenumber"].ToString();
                        comp.address = dtResult.Rows[i]["address"].ToString();
                        comp.emailid = dtResult.Rows[i]["emailid"].ToString();
                        comp.logo = dtResult.Rows[i]["logo"].ToString();
                        comp.points_ratio = convertToDecimal(dtResult.Rows[i]["inr_point_ratio"].ToString());
                        comp.point_range_min = convertToDecimal(dtResult.Rows[i]["point_range_min"].ToString());
                        comp.point_range_max = convertToDecimal(dtResult.Rows[i]["point_range_max"].ToString());
                        comp.root_url_1 = dtResult.Rows[i]["root_url_1"].ToString();
                        comp.root_url_2 = dtResult.Rows[i]["root_url_2"].ToString();
                        comp.shipping_charge = convertToDecimal(dtResult.Rows[i]["shipping_charge"].ToString());
                        comp.min_order_for_free_shipping = convertToDecimal(dtResult.Rows[i]["min_order_for_free_shipping"].ToString());
                       
                        comp.sales_tracking = Convert.ToInt32(dtResult.Rows[i]["sales_tracking"].ToString());
                        comp.track_cycle = dtResult.Rows[i]["track_cycle"].ToString();
                        comp.validity = Convert.ToInt32(dtResult.Rows[i]["validity"].ToString());
                         
                        lcomp.Add(comp);
                    }
                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return lcomp;
        }

        #endregion

        #region=========Category=================
        public string CategoryEntry(CATEGORY cate)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                int i = da.ExecuteSP("category_insert", ref oc
                                , da.Parameter("_CAT_ID", cate.CAT_ID)
                                , da.Parameter("_CAT_NAME", cate.CAT_NAME)
                                , da.Parameter("_CAT_DISPLAY_NAME", cate.CAT_DISPLAY_NAME)
                                , da.Parameter("_CAT_DESCRIPTION", cate.CAT_DESCRIPTION)
                                , da.Parameter("_PARENT_CAT_ID", cate.PARENT_CAT_ID)
                                , da.Parameter("_CAT_IMG_URL", cate.CAT_IMG_URL)
                                , da.Parameter("_ACTION_FLAG", 1)
                                , da.Parameter("_OUTMSG", String.Empty, true)
                                );

                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }
        #endregion

      

        #region=========Product=================
        public string ProductEntry(PRODUCT prod)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                int i = da.ExecuteSP("product_insert", ref oc
                                , da.Parameter("_PRODUCT_ID", prod.PRODUCT_ID)
                                , da.Parameter("_PRODUCT_NAME", prod.PRODUCT_NAME)
                                , da.Parameter("_PRODUCT_DISP_NAME", prod.PRODUCT_DISP_NAME)
                                , da.Parameter("_PRODUCT_DESC", prod.PRODUCT_DESC)
                                , da.Parameter("_MRP", prod.MRP)
                                , da.Parameter("_LIST_PRICE", prod.LIST_PRICE)
                                , da.Parameter("_MIN_SALE_PRICE", prod.MIN_SALE_PRICE)
                                , da.Parameter("_PRODUCT_IMG_URL", prod.PRODUCT_IMG_URL)
                                , da.Parameter("_ACTION_FLAG", 1)
                                , da.Parameter("_OUTMSG", String.Empty, true)
                                );

                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }
        #endregion
    }
}
