﻿﻿using AttributeRouting.Web.Http;
using ecomm.model.repository;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ecomm.api.Controllers
{
    public class giftController : ApiController
    {

        [POST("/gv/insert/")]
        public string post_user_gift_voucher_insert(gift gft)
        {
            gift_repository gr = new gift_repository();
            return gr.insert_gv(gft);//.GiftVoucherInsert(gft);
        }

        [GET("/gv/update/{gift_code}/{status}")]
        public string get_user_gift_voucher_update(string gift_code, string status)
        {
            gift_repository gr = new gift_repository();
            return gr.update_gv_status(gift_code, status);
        }

        [GET("/gv/details_by_status/{gift_code}/{status}")]
        public string get_user_gift_voucher_bal(string gift_code, string status)
        {
            gift_repository gr = new gift_repository();
            return gr.get_gv_by_code(gift_code, status);
        }
        [GET("/gv/active/list_by_user?user={user}")]
        public string get_active_gift_voucher_by_user(string user)
        {
            gift_repository gr = new gift_repository();
            return gr.get_gv_by_code(user, "0"); // check only available balance
        }
        [GET("/gv/all/list_by_user?user={user}")]
        public string get_all_gift_voucher_by_user(string user)
        {
            gift_repository gr = new gift_repository();
            return gr.get_gv_all_by_user(user); // check only available balance
        }
        [GET("/gv/details/{gv}")]
        public gift get_gv_details(string gv)
        {
            gift_repository gr = new gift_repository();
            return gr.get_gv_details(gv); // check only available balance
        }

        [GET("/gv/get_all_voucher_code/{gv_status}")]
        public List<gift> get_all_voucher_code(string gv_status)
        {
            gift_repository gr = new gift_repository();
            return gr.get_all_voucher_code(gv_status);
        }

        [GET("/gv/get_find_voucher_code?gv_code={gv_code}&store={store}")]
        public gv_voucher get_find_voucher_code(string gv_code, string store)
        {
            gift_repository gr = new gift_repository();
            return gr.get_find_voucher_code(gv_code, store);
        }

        [GET("/gv/get_GV_Voucher_Details?store={store}&fromdt={fromdt}&todt={todt}")]
        //public List<gift> get_GV_Voucher_Details(string store, string fromdt, string todt)
        public List<gv_voucher> get_GV_Voucher_Details(string store, string fromdt, string todt)
        {
            gift_repository gr = new gift_repository();
            return gr.get_GV_Voucher_Details(store, fromdt, todt);
        }

        [GET("/gv/get_GV_Voucher_Report?store={store}&fromdt={fromdt}&todt={todt}")]
        public string get_GV_Voucher_Report(string store, string fromdt, string todt)
        {
            gift_repository gr = new gift_repository();
            return gr.get_GV_Voucher_Report(store, fromdt, todt);
        }

        [POST("/gv/postExportVouList/")]
        public string postExportVouList(expo_vou_grid evg)
        {
            ecomm.model.repository.gift_repository gr = new model.repository.gift_repository();
            return gr.postExportVouList(evg);
        }


        [POST("/gv/update/post_redeem_voucher_code/")]
        public string post_redeem_voucher_code(gift gf)
        {
            gift_repository gr = new gift_repository();
            return gr.post_redeem_voucher_code(gf);
        }

        [POST("/gv/pay_by_gv_points/")]
        public string post_user_gift_point_status(payment_points_gifts ppg)
        {
            payment_repository pr = new payment_repository();

            double totAmt = pr.getOrderAmount(Convert.ToInt64(ppg.OrderID));
            string strPointsAmt = ppg.Points_INR;
            string strGVAmt = ppg.egiftVouAmt;
            string strDiscount = ppg.discount_coupon;
            string strDiscount_amount = ppg.discount_amount;
            if (string.IsNullOrEmpty(strPointsAmt))
                strPointsAmt = "0";
            if (string.IsNullOrEmpty(strGVAmt))
                strGVAmt = "0";
            if (string.IsNullOrEmpty(strDiscount_amount))
                strDiscount_amount = "0";



            if (Convert.ToDouble(strPointsAmt) + Convert.ToDouble(strGVAmt) + Convert.ToDouble(strDiscount_amount) >= totAmt)
            {

                Boolean bln = pr.PayWithPointsGV(ppg.EmailID, ppg.OrderID, ppg.PaidAmt, ppg.Points, ppg.Points_INR, ppg.strActualAmt, ppg.egiftVouNo, ppg.egiftVouAmt, ppg.discount_coupon,ppg.discount_amount, ppg.status);
                if (bln)
                    return "{'ret':'Success'}";
                else
                    return "{'ret':'Failure'}";
            }
            else
                return "{'ret'':'Failure}";
        }

        [GET("/gv/validate/{gift_code}/{store}")]
        public gift get_validate_gv(string gift_code, string store)
        {
            gift_repository gr = new gift_repository();
            return gr.validate_gv(gift_code, store);
        }



    }
}
